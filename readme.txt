image: 			    lede-x86-64-combined-squashfs.img
firmware upgrade : 	sysupgrade -c -v ./lede-x86-64-combined-squashfs.img

default ip:         192.168.1.1
defined in          /etc/config/network

ratechecker:
-compile on local computer as described on bitbucket
-you may need to install some packets
                    libevent-dev
                    libevent-2.0-5
-copy to apu        scp <source> root@192.168.1.1:
-run manually       ./ratechecker -c -d -g <#nodes> -C <frequency> -n <#rounds>


autostart :
files : 		    ratechecker --> /root/
                    autostart.py --> /root/
			        runratechecker --> /etc/init.d/

configuration:
-make it executable	chmod +x /etc/init.d/runratechecker
-enable autostart	/etc/init.d/runratechecker enable
-check symlink		ls -lh /etc/rc.d | grep runratechecker
-confirm 	        /etc/init.d/runratechecker enabled && echo on

-kill process       killall ratechecker
