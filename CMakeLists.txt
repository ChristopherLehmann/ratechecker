cmake_minimum_required(VERSION 2.8)
project(ratechecker)

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/bin" )
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -W -Wextra -ggdb")

find_library(
        EVENT libevent.a
)

set(SOURCE_FILES
        main.c
        monitor.c
        radiotap.c
        net_radiotap.c
        ieee80211.c
        skb.c
        )

add_executable(ratechecker ${SOURCE_FILES})
target_link_libraries(
        ratechecker
        ${EVENT}
        m
)

install(TARGETS ratechecker DESTINATION /tmp/)

set(CPACK_PACKAGE_NAME "ratechecker")
set(CPACK_PACKAGE_VERSION "1.0.0")

include(CPack)
