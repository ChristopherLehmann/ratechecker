# Ratechecker 
### A simple tool to comprehensively quantify your wifi network links

## What does it do?
Ratechecker is a program that you can run in your ad-hoc wifi network to "thoroughly" measure the quality of the links of your network. (By default) each node sends a series of packets in all possible modulation rates. The program also acts as the receiver simultaneously and log the packets received or lost as a bitmap.

## Why is this better than just persistent ping / iperf to measure losses?
Ping / Iperf or any other typical network tool sends packets in the [modulation rate](https://en.wikipedia.org/wiki/IEEE_802.11n-2009#Data_rates) decided by the rate-control algorithm like *Minstrel*. The information about the loss rates in all other modulation rates is essentially not recorded. Ratechecker, on the other hand, deliberately injects wifi packets in all the modulation rates and thus records a more comprehensive view of the network.

## How to run the measurement?
Simply start the *ratechecker* in each node of the ( wifi mesh) network and let it do the magic. Stop the program after about an hour and the measurement log files will be written to the `/tmp/<your-mac-addr>/` folder.It generates a JSON file for each neighbour comprising a bitmap of the received/lost packets in each *round*.  

## What can I do with the created measurement logs?
The measurement log can be useful for network engineers designing an alternative routing protocol or a rate-control algorithm.
It can also be used to visualise the network using this (TODO link) parser to generate a network graph based on Dijkstra algorithm and ETX metric.

# Usage

```
#!shell

ratechecker [-p phy][-i ibss] [-r send/recv/duplex] [-t interval] [-n max_rounds] [-m]
	Options:
		 -p 	phy interface to use;                  Default: Phy0
		 -i 	ibss interface to use;                 Default: wlan0
		 -r 	Role of the node;                      Default: duplex
		 -t 	interval between sending each batch;   Default: 1s
		 -n 	number of rounds to send;              Default: 16
		 -m 	Send only in mcs rates

```

# Build


```
#!shell

cmake .
make
make install      # The binary will be installed in the /tmp directory
```

## Limitations
The tool can currently run only on devices that use **a recent version of ath9k** driver.